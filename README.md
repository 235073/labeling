# labeling

## Getting started

Pierwsze odpalenie
```commandline

python labeling.py

```


Drugie odpalenie

```commandline

python labeling.py labeled.csv

```

Wtedy będzie kontynuować z ostatniego tweeta.

Możecie ustalić własne przyciski do labelowania:

```python

POSITIVE_CHAR = "D"
NEUTRAL_CHAR = "S"
NEGATIVE_CHAR = "A"


```