import sys

import pandas
import os


def clear_console():
    command = 'clear' # lub 'cls' jeśli pracujecie na windowsie
    os.system(command)


POSITIVE_CHAR = "D"
NEUTRAL_CHAR = "S"
NEGATIVE_CHAR = "A"


def get_label(input_value):
    switcher = {
        POSITIVE_CHAR: 1,
        NEGATIVE_CHAR: -1,
        NEUTRAL_CHAR: 0
    }
    return switcher.get(input_value)


file = sys.argv[1] if len(sys.argv) == 2 else "filtered.csv"
df = pandas.read_csv(file)
rest = pandas.DataFrame()
if "checked" in df.columns:
    df = df.loc[df["checked"] == 0]
    rest = df.loc[df["checked"] == 1]

df["label"] = 0
df["checked"] = False


for index, row in df.iterrows():
    clear_console()
    print("________")
    print(row.tweet_id)
    print("________")
    print(row.text)
    print("________")
    print(f"LABEL '{NEGATIVE_CHAR}' - negative, '{NEUTRAL_CHAR}' - neutral, '{POSITIVE_CHAR}' - positive, end - end\n")
    # label = input()
    label = get_label(input())
    if label not in [0, 1, -1]:
        break
    df.at[index, "label"] = label
    df.at[index, "checked"] = True

df = pandas.concat([rest, df])
df.to_csv("labeled.csv", index=False)

