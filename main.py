import re
from pathlib import Path

from itertools import repeat

import numpy as np
import pandas as pd
import scipy.sparse.csr
from matplotlib import pyplot as plt
from modAL.models import ActiveLearner
from modAL.uncertainty import uncertainty_sampling, margin_sampling, entropy_sampling, classifier_entropy, classifier_margin
from modAL.utils import multi_argmax
from modAL.utils.selection import shuffled_argmax
from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.metrics import accuracy_score, f1_score, balanced_accuracy_score
from sklearn.naive_bayes import MultinomialNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

TEST_SIZE = 39
STOP_WORDS = True
CLEAN_TWEETS = True
BATCH_SIZE = 18
INIT_N = 15


def clean_tweet(tweet: str) -> str:
    temp = tweet.lower()
    temp = re.sub("'", "", temp)  # to avoid removing contractions in english
    temp = re.sub("@[A-Za-z0-9_]+", "", temp)
    temp = re.sub("#[A-Za-z0-9_]+", "", temp)
    temp = re.sub(r"http\S+", "", temp)
    temp = re.sub("[()!?]", " ", temp)
    temp = re.sub("\[.*?\]", " ", temp)
    temp = re.sub("[^a-z0-9]", " ", temp)
    temp = temp.strip()
    return temp


df = pd.read_csv(
    Path.cwd() / "labeled_part.csv",
    encoding="utf-8",
)
if CLEAN_TWEETS:
    df.text = df.text.apply(clean_tweet)


def get_init_test_train(
    df: pd.DataFrame, init_n: int, random_state: int
) -> tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    dffs = []
    for label, dff in df.groupby("label"):
        dffs.append(dff.sample(n=init_n, random_state=random_state))

    init_df = pd.concat(dffs)
    df = df.drop(init_df.index).reset_index(drop=True)
    init_df = init_df.reset_index(drop=True)

    test_df, train_df = (
        df[:TEST_SIZE].reset_index(drop=True),
        df[TEST_SIZE:].reset_index(drop=True),
    )

    return init_df, test_df, train_df


init_df, test_df, train_df = get_init_test_train(df, init_n=INIT_N, random_state=42)

vectorizer = CountVectorizer(
    input="content",
    lowercase=True,
    stop_words="english" if STOP_WORDS else None,
    analyzer="word",
    ngram_range=(1, 2),
    strip_accents="ascii",
).fit(df["text"])
transformer = TfidfTransformer(use_idf=False).fit(vectorizer.transform(df["text"]))


def transform_data(df: pd.DataFrame) -> tuple[scipy.sparse.csr.csr_matrix, np.ndarray]:
    X_counts = vectorizer.transform(df["text"])
    Y = df.label.to_numpy()
    X = transformer.transform(X_counts)

    return X, Y


x_init, y_init = transform_data(init_df)
x_train, y_train = transform_data(train_df)
x_test, y_test = transform_data(test_df)

x_test, x_init = x_test.toarray(), x_init.toarray()

def show_acc_plot(name: str, query_results: dict[str, list[float]], y_name: str):
    whole_set_size = len(train_df) + len(init_df)
    x_ticks = [min(len(init_df) + i * BATCH_SIZE, whole_set_size) for i in range(len(query_results['random']))]
    x_ticks = [int(v / whole_set_size * 100) for v in x_ticks]

    percentage_markers = [100]
    rand_results = dict(zip(x_ticks, query_results['random']))

    with plt.style.context("seaborn-white"):
        plt.figure(figsize=(10, 3))

        for query_name, results in query_results.items():
            if query_name == 'random':
                continue
            # XD
            # plt.plot(range(1, len(results) + 1), results, label=query_name)
            # plt.scatter(range(1, len(results) + 1), results, label="_nolegend_")
            plt.plot(x_ticks, results, label=query_name)
            plt.scatter(x_ticks, results, label="_nolegend_")

        for percentage in percentage_markers:
            lists = sorted(dict(zip(x_ticks, repeat(rand_results[percentage]))).items())
            x, y = zip(*lists)
            plt.plot(x,y, label=f"{percentage}%", alpha=0.3)
            # plt.fill_between(x[:5], x[:5], alpha=0.1)

        # plt.legend()
        plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

        # plt.title(f"Classifier: {name}, Batch Size: {BATCH_SIZE}")
        plt.title(f"Classifier: {name}")
        plt.xlabel("Percentage of whole training set used (%)")
        plt.ylabel(y_name)
        plt.tight_layout()
        # plt.xticks(list(range(1, len(results) + 1)))

        plt.savefig(f"./plots/{y_name.lower().replace(' ','_')}_{name}", bbox_inches="tight", dpi=300)
        plt.close("all")


def train_learner(
    learner: ActiveLearner,
    x_train: np.array,
    y_train: np.array,
    x_test: np.array,
    y_test: np.array,
) -> list[float]:
    x_copy, y_copy = x_train.toarray(), np.copy(y_train)
    f1_scores = []
    accs = []

    for _ in range(len(x_copy) // BATCH_SIZE):
        query_idx, query_inst = learner.query(
            x_copy,
            random_tie_break=True,
            n_instances=BATCH_SIZE,
        )

        # Teach with manually assigned class
        learner.teach(query_inst, y_copy[query_idx])

        # Remove labeled
        x_copy = np.delete(x_copy, query_idx, axis=0)
        y_copy = np.delete(y_copy, query_idx, axis=0)

        prediction = learner.predict(x_test)
        f1_scores.append(f1_score(y_test, prediction, average="weighted"))
        accs.append(balanced_accuracy_score(y_test, prediction))

    # Last batch
    learner.teach(x_copy, y_copy)
    prediction = learner.predict(x_test)
    f1_scores.append(f1_score(y_test, prediction, average="weighted"))
    accs.append(balanced_accuracy_score(y_test, prediction))

    return f1_scores, accs


def random_sampling(
    classifier,
    X: np.array,
    n_instances: int = 1,
    random_tie_break: bool = False,
    **uncertainty_measure_kwargs,
) -> np.ndarray:
    return np.random.choice(X.shape[0], n_instances, replace=False)


def margin_entropy_sampling(
    classifier,
    X: np.array,
    n_instances: int = 1,
    random_tie_break: bool = False,
    **uncertainty_measure_kwargs,
) -> np.ndarray:
    entropy = classifier_entropy(classifier, X, **uncertainty_measure_kwargs)
    margin = classifier_margin(classifier, X, **uncertainty_measure_kwargs)

    if not random_tie_break:
        return multi_argmax(entropy-margin, n_instances=n_instances)
    return shuffled_argmax(entropy-margin, n_instances=n_instances)


names = [
    "Nearest Neighbors",
    # "Linear SVM",
    # "RBF SVM",
    "Decision Tree",
    "Random Forest",
    # "Neural Net",
    # "AdaBoost",
    # "Naive Bayes",
    "SGD",
]
classifiers = [
    KNeighborsClassifier(3),
    # SVC(kernel="linear", C=0.025, probability=True),
    # SVC(gamma=2, C=1, probability=True),
    DecisionTreeClassifier(max_depth=5),
    RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
    # MLPClassifier(alpha=1, max_iter=1000),
    # AdaBoostClassifier(),
    # MultinomialNB(),
    SGDClassifier(
        loss="log", penalty="l2", alpha=1e-3, random_state=42, max_iter=5, tol=None
    ),
]

query_names = ["random", "uncertainty", "margin", "entropy", "margin-entropy"]
query_strategies = [
    random_sampling,
    uncertainty_sampling,
    margin_sampling,
    entropy_sampling,
    margin_entropy_sampling,
]

def get_data(random_state: int):
    init_df, test_df, train_df = get_init_test_train(df, init_n=INIT_N, random_state=random_state)

    x_init, y_init = transform_data(init_df)
    x_train, y_train = transform_data(train_df)
    x_test, y_test = transform_data(test_df)

    x_test, x_init = x_test.toarray(), x_init.toarray()

    return x_init, y_init, x_train, y_train, x_test, y_test

N_TRIALS = 50
acc_dfs = []
f1_dfs = []

for i in range(N_TRIALS):
    x_init, y_init, x_train, y_train, x_test, y_test = get_data(i)

    for name, estimator in zip(names, classifiers):
        query_results_f1 = {}
        query_results_acc = {}

        for query_name, strategy in zip(query_names, query_strategies):
            learner = ActiveLearner(
                estimator=estimator,
                query_strategy=strategy,
                X_training=x_init,
                y_training=y_init,
            )
            # ACC only on INIT SET
            prediction = learner.predict(x_test)
            query_results_f1[query_name] = [f1_score(y_test, prediction, average="weighted")]
            query_results_acc[query_name] = [balanced_accuracy_score(y_test, prediction)]

            f1_scores, accs = train_learner(
                learner=learner,
                x_train=x_train,
                y_train=y_train,
                x_test=x_test,
                y_test=y_test,
            )
            query_results_f1[query_name].extend(f1_scores)
            query_results_acc[query_name].extend(accs)

        f1_df = pd.DataFrame.from_dict(query_results_f1)
        f1_df['trial'] = i
        f1_df['clf'] = name
        f1_df = f1_df.reset_index().rename({'index': 'batch_num'}, axis=1)

        acc_df = pd.DataFrame.from_dict(query_results_acc)
        acc_df['trial'] = i
        acc_df['clf'] = name
        acc_df = acc_df.reset_index().rename({'index': 'batch_num'}, axis=1)

        f1_dfs.append(f1_df)
        acc_dfs.append(acc_df)
        # show_acc_plot(name, query_results_f1, y_name='F1 Score')
        # show_acc_plot(name, query_results_acc, y_name='Balanced Accuracy')


f1_df = pd.concat(f1_dfs).reset_index(drop=True)
acc_df = pd.concat(acc_dfs).reset_index(drop=True)

def get_mean_df(df: pd.DataFrame) -> pd.DataFrame:
    series = []
    for (clf, batch_num), df in df.groupby(['clf', 'batch_num']):
        res = df[query_names].mean()
        res['clf'] = clf
        res['batch_num'] = batch_num
        series.append(res)
    return pd.DataFrame(series)

mean_f1 = get_mean_df(f1_df)
mean_acc = get_mean_df(acc_df)

for clf, dff in mean_f1.groupby('clf'):
    query_results = {
        query_name : dff[query_name].to_list() for query_name in query_names
    }
    show_acc_plot(name=clf, query_results=query_results, y_name='F1 Score')

for clf, dff in mean_acc.groupby('clf'):
    query_results = {
        query_name : dff[query_name].to_list() for query_name in query_names
    }
    show_acc_plot(name=clf, query_results=query_results, y_name='Balanced Accuracy')


"""
Poprawki techniczne do skryptu:
* Wiele eksperymentów per rodzaj (np. 50 eksperymentow z wykorzystaniem 25% danych dla okreslonego rodzaju samplingu)
* Obszar od random 100 procent (zaznaczone na wykresie kiedy sampling dziala lepiej niz random 100%, 50% itd.) Done
* Opis osi  X jako procent całego zbioru (nie ilosc batchy, tylko % calego zbioru danych) Done
* Mieszane strategie active learning (to bym ominal) Done
* Zrobic tez diagramy dla balanced accuracy (a nie tylko f1 score) Done
* Tylko 3 clf a nie wszystkie Done

* Moze jednak zmniejszyc init_set, tak aby query_strategy mialo wieksza szanse sie wykazac?
"""